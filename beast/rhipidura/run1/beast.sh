#! /bin/sh
#$ -S /bin/bash
#$ -cwd
#$ -V
#$ -l h_vmem=2G
#$ -l vf=2G
#$ -q all.q

source ~/.bash_profile
cd /share/work1
cd $SGE_O_WORKDIR

/home/jamie/Environment/bin/beast -seed 94889653 *.xml
