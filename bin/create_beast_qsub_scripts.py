#! /usr/bin/env python

import os
import sys
import glob
import random

import project_util
from project_util import get_qsub_preamble

_LOG = project_util.get_logger(__name__, level='info')


def main():
    rng = random.Random()
    rng.seed(934785)
    beast_path = '/home/jamie/Environment/bin/beast'
    beast_dirs = glob.glob(os.path.join(project_util.BEAST_DIR,
        '*', 'run*'))
    qsub_header = get_qsub_preamble()
    for d in beast_dirs:
        seed = rng.randint(1,99999999)
        qsub = qsub_header + '\n' + beast_path + ' -seed ' + str(seed) + ' *.xml\n'
        qpath = os.path.join(d, 'beast.sh')
        out = open(qpath, 'w')
        out.write(qsub)
        out.close()

if __name__ == '__main__':
    main()

