#! /usr/bin/env python

import os
import sys
import glob
import random

import project_util
from project_util import get_qsub_preamble

_LOG = project_util.get_logger(__name__, level='info')


def main():
    sumtrees_path = '/home/jamie/Environment/bin/sumtrees.py'
    beast_dirs = glob.glob(os.path.join(project_util.BEAST_DIR,
        '*'))
    qsub_header = get_qsub_preamble()
    for d in beast_dirs:
        taxon = os.path.basename(d)
        out_path = taxon + '.mrc.tre'
        cmd = '{0} -b 1001 --ultrametric -e mean-age -f 0.0 -o {1} `find . -name "*.trees"`'.format(
                sumtrees_path,
                out_path)
        qsub = qsub_header + '\n' + cmd + '\n'
        qpath = os.path.join(d, 'sumtrees.sh')
        out = open(qpath, 'w')
        out.write(qsub)
        out.close()

if __name__ == '__main__':
    main()

