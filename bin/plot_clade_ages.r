rm(list=ls(all=TRUE))
# library(gridExtra)
# require(grDevices)
# require(graphics)
library(ggplot2)

# vplayout <- function(x, y){viewport(layout.pos.col = x, layout.pos.row = y)}

data_path = 'clade_age_data.txt'
raw_data = read.csv(data_path, header=TRUE, sep='\t')
dat = subset(raw_data, numspecies > 1)
dat = subset(dat, !is.na(age))
dat$type = factor(dat$type, levels = c(levels(dat$type), 'unclassified'))
dat$type[dat$type == ''] = 'unclassified'
dat$jnumspecies = jitter(dat$numspecies)
dat$lognumspecies = log10(dat$numspecies)
dat$jlognumspecies = jitter(dat$lognumspecies)

reg = lm(formula = dat$numspecies ~ dat$age)
r_squared = summary(reg)$r.squared
p_value = anova(reg)[['Pr(>F)']][1]
slope = reg$coefficients[2]

x_min = min(dat$lowerci[!is.na(dat$lowerci)])
x_max = max(dat$upperci[!is.na(dat$upperci)])
x_buffer = 0.02 * (x_max - x_min)
x_min = x_min - x_buffer
x_max = x_max + x_buffer
y_min = min(dat$numspecies)
y_max = max(dat$numspecies)
y_buffer = 0.02 * (y_max - y_min)
y_min = y_min - y_buffer
y_max = y_max + y_buffer
type_map = list()
group_map = list()
types = unique(as.vector(dat$type))
groups = unique(as.vector(dat$group))
# color_palette = rainbow(length(types))
# color_palette = topo.colors(length(types))
color_palette = colors()[c(24, 555, 132, 466)]
char_palette = 1:4
char_palette = c(1,2,4,6)
for (i in 1:length(types)) {
    # type_map[types[i]] = color_palette[i]
    type_map[types[i]] = color_palette[1]
}
for (i in 1:length(groups)) {
    group_map[groups[i]] = char_palette[i]
}

plot_letters = LETTERS[1:2]
pdf('age_vs_diversity.pdf', width=8, height=9)
# par(yaxs = "r", xaxs = "r")
par(yaxs = "i", xaxs = "i")
# oma is space around all plots
par(cex.axis=0.9, cex.lab=1.1)
par(mgp = c(1.7,0.6,0)) # mgp adjusts space of axis labels
# oma is space around all plots
par(oma = c(0,0,0.65,0.5), mar = c(3,3,0.3,0))
par(mfrow = c(2, 1))

plot(dat$age, dat$numspecies,
        # col = dat$type,
        # pch = dat$group,
        type = 'n',
     	xlim = c(x_min, x_max),
        ylim = c(0, y_max),
	    # xlab = "Estimated time of colonization (mya)",
	    xlab = "",
        ylab = "Number of species",
        main = "")
for (i in 1:length(dat$age)) {
    if ((!is.na(dat[i,]$lowerci)) & (!is.na(dat[i,]$upperci))) {
        lines(c(dat[i,]$lowerci, dat[i,]$upperci),
              c(dat[i,]$jnumspecies, dat[i,]$jnumspecies),
              col = 'grey50')
    }
}
print(warnings())
for (t in types) {
    for (g in groups) {
        plot_data = subset(dat, ((type == t) & (group == g)))
        points(plot_data$age, plot_data$jnumspecies,
               col = type_map[[t]],
               pch = group_map[[g]],
               cex = 1.1,
               lwd = 2)
    }
}
abline(reg, lty=5, col='grey70')
mtext(bquote(italic(r^2) == .(round(r_squared, 3))),
      side = 3,
      adj = 0.99,
      line = -1.1,
      cex = 0.8)
mtext(bquote(hat(beta) == .(round(slope, 3))),
      side = 3,
      adj = 0.99,
      line = -2.15,
      cex = 0.8)
mtext(bquote(italic(P) == .(round(p_value, 3))),
      side = 3,
      adj = 0.99,
      line = -2.9,
      cex = 0.8)
mtext(plot_letters[[1]], side=3, adj=0, line=0, cex=1.2, font=2)
# legend(x = 0.718 * x_max, y = 0.88559 * y_max,
#        legend = types,
#        fill = color_palette,
#        cex = 0.8,
#        bty = 'n')
legend(x = 0.873 * x_max, y = 0.88559 * y_max,
       legend = groups,
       pch = char_palette,
       cex = 0.8,
       pt.lwd = 2,
       bty = 'n')
# mtext(plot_letters[[t]], side=3, adj=0, line=0.2, cex=0.75, font=2)
# mtext(bquote(tau *" ~ "* italic(U)(0,.(t))), side=3, adj=0.1, line=0.1, cex=0.6)
# mtext(bquote(italic(p)(hat(Omega)<Omega) == .(p_round)), side=3, adj=1, line=0.1, cex=0.6)
# mtext(bquote(hat(Omega) * phantom(0) * (.(s))), WEST<-2, padj=0.1, cex=1.3, outer = T)
# mtext(bquote("True" * phantom(0) * Omega), SOUTH<-1, padj=0, cex=1.3, outer = T)

# dev.off()

reg = lm(formula = dat$lognumspecies ~ dat$age)
r_squared = summary(reg)$r.squared
p_value = anova(reg)[['Pr(>F)']][1]
slope = reg$coefficients[2]

x_min = min(dat$lowerci[!is.na(dat$lowerci)])
x_max = max(dat$upperci[!is.na(dat$upperci)])
x_buffer = 0.02 * (x_max - x_min)
x_min = x_min - x_buffer
x_max = x_max + x_buffer
y_min = min(dat$lognumspecies)
y_max = max(dat$lognumspecies)
y_buffer = 0.02 * (y_max - y_min)
y_min = y_min - y_buffer
y_max = y_max + y_buffer
type_map = list()
group_map = list()
types = unique(as.vector(dat$type))
groups = unique(as.vector(dat$group))
# color_palette = rainbow(length(types))
# color_palette = topo.colors(length(types))
color_palette = colors()[c(24, 555, 132, 466)]
char_palette = 1:4
char_palette = c(1,2,4,6)
for (i in 1:length(types)) {
    # type_map[types[i]] = color_palette[i]
    type_map[types[i]] = color_palette[1]
}
for (i in 1:length(groups)) {
    group_map[groups[i]] = char_palette[i]
}

# pdf('age_vs_log_diversity.pdf', width=8, height=6)
# par(yaxs = "r", xaxs = "r")
# par(yaxs = "i", xaxs = "i")
# # oma is space around all plots
# par(cex.axis=0.9, cex.lab=1.1)
# par(mgp = c(1.7,0.6,0)) # mgp adjusts space of axis labels
# # oma is space around all plots
# par(oma = c(0,0,0,0.5), mar = c(3,3,0.3,0))

plot(dat$age, dat$lognumspecies,
        # col = dat$type,
        # pch = dat$group,
        type = 'n',
     	xlim = c(x_min, x_max),
        ylim = c(y_min, y_max),
	    xlab = "Estimated time of colonization (mya)",
        ylab = bquote(log[10](Number~of~species)),
        main = "")
for (i in 1:length(dat$age)) {
    if ((!is.na(dat[i,]$lowerci)) & (!is.na(dat[i,]$upperci))) {
        lines(c(dat[i,]$lowerci, dat[i,]$upperci),
              c(dat[i,]$jlognumspecies, dat[i,]$jlognumspecies),
              col = 'grey50')
    }
}
print(warnings())
for (t in types) {
    for (g in groups) {
        plot_data = subset(dat, ((type == t) & (group == g)))
        points(plot_data$age, plot_data$jlognumspecies,
               col = type_map[[t]],
               pch = group_map[[g]],
               cex = 1.1,
               lwd = 2)
    }
}
abline(reg, lty=5, col='grey70')
mtext(bquote(italic(r^2) == .(round(r_squared, 3))),
      side = 3,
      adj = 0.99,
      line = -1.1,
      cex = 0.8)
mtext(bquote(hat(beta) == .(round(slope, 3))),
      side = 3,
      adj = 0.99,
      line = -2.15,
      cex = 0.8)
mtext(bquote(italic(P) == .(round(p_value, 3))),
      side = 3,
      adj = 0.99,
      line = -2.9,
      cex = 0.8)
mtext(plot_letters[[2]], side=3, adj=0, line=0, cex=1.2, font=2)
# legend(x = 0.718 * x_max, y = 0.88559 * y_max,
#        legend = types,
#        fill = color_palette,
#        cex = 0.8,
#        bty = 'n')
legend(x = 0.873 * x_max, y = 0.88559 * y_max,
       legend = groups,
       pch = char_palette,
       cex = 0.8,
       pt.lwd = 2,
       bty = 'n')
# mtext(plot_letters[[t]], side=3, adj=0, line=0.2, cex=0.75, font=2)
# mtext(bquote(tau *" ~ "* italic(U)(0,.(t))), side=3, adj=0.1, line=0.1, cex=0.6)
# mtext(bquote(italic(p)(hat(Omega)<Omega) == .(p_round)), side=3, adj=1, line=0.1, cex=0.6)
# mtext(bquote(hat(Omega) * phantom(0) * (.(s))), WEST<-2, padj=0.1, cex=1.3, outer = T)
# mtext(bquote("True" * phantom(0) * Omega), SOUTH<-1, padj=0, cex=1.3, outer = T)

dev.off()

pdf('age_vs_diversity_gg.pdf')
p = ggplot(dat)
p = p + geom_errorbarh(data = dat,
                   aes(x = age, y = numspecies,
                       xmin = lowerci, xmax = upperci,
                       height = 0.5),
                   colour = 'grey40')
p = p + geom_point(aes(x = age, y = numspecies,
                       colour = factor(type),
                       shape = factor(group)),
                   size=3)
p
dev.off()

plot.split.ages = function(d, key="Title", key_label="", shape=16, size=1) {
        d$key = d[,key]
    p = ggplot(d)
        p = p + aes(x=key, y=MeanAge, ymin=AgeHPD1, ymax=AgeHPD2)
        p = p + geom_pointrange()
            if (key_label == "") {
                        key_label = key
            }
            p = p + labs(y="Age (mybp)", x=key_label)
                p = p + coord_flip()
                return(p)
}



