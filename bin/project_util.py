#! /usr/bin/env python

import os
import sys
import logging

BIN_DIR = os.path.abspath(os.path.dirname(__file__))
PROJECT_DIR = os.path.abspath(os.path.dirname(BIN_DIR))
DATA_DIR = os.path.abspath(os.path.join(PROJECT_DIR, 'data'))
RAW_DATA_DIR = os.path.abspath(os.path.join(DATA_DIR, 'files-from-others'))
BEAST_DIR = os.path.abspath(os.path.join(PROJECT_DIR, 'beast'))

def get_qsub_preamble(memory = "2G", queue = "all.q"):
    return """#! /bin/sh
#$ -S /bin/bash
#$ -cwd
#$ -V
#$ -l h_vmem=%s
#$ -l vf=%s
#$ -q %s

source ~/.bash_profile
cd /share/work1
cd $SGE_O_WORKDIR
""" % (memory, memory, queue)

def get_logging_level(level=None):
    if level:
        if level.upper() == "NOTSET":
            return logging.NOTSET
        elif level.upper() == "DEBUG":
            return logging.DEBUG
        elif level.upper() == "INFO":
            return logging.INFO
        elif level.upper() == "WARNING":
            return logging.WARNING
        elif level.upper() == "ERROR":
            return logging.ERROR
        elif level.upper() == "CRITICAL":
            return logging.CRITICAL
        else:
            return logging.WARNING
    else:
        return logging.WARNING

def get_logger(name = os.path.basename(PROJECT_DIR), level=None):
    l = get_logging_level(level)
    log = logging.getLogger(name)
    log.setLevel(l)
    h = logging.StreamHandler()
    h.setLevel(l)
    log.addHandler(h)
    return log

def main():
    sys.stdout.write("%s" % PROJECT_DIR)

if __name__ == '__main__':
    main()

