#! /usr/bin/env python

import os
import sys
import glob
import re
import csv
from xml.etree import ElementTree

import dendropy

import project_util
from project_util import get_qsub_preamble

_LOG = project_util.get_logger(__name__, level='info')

def get_dict_reader(source, delimiter='\t', header_offset=0):
    if isinstance(source, str):
        source = open(source, 'rU')
    [source.next() for i in range(header_offset)]
    return csv.DictReader(source, delimiter='\t')

def match_iter(fields, patterns):
    return (f for f in fields if any(p.match(f) for p in patterns))
            
def get_parameter_means(logs, delimiter='\t', header_offset=0, burnin=0,
        parameter_patterns=[]):
    field_names = get_dict_reader(logs[0], delimiter=delimiter,
            header_offset=header_offset).fieldnames
    parameter_names = field_names
    if parameter_patterns and len(parameter_patterns) > 0:
        parameter_names = list(match_iter(field_names, parameter_patterns))
    parameter_sums = dict(zip(parameter_names, [0.0 for i in parameter_names]))
    count = 0
    for log in logs:
        if isinstance(log, str):
            log = open(log, 'rU')
        dr = get_dict_reader(log, delimiter=delimiter, header_offset=header_offset)
        assert sorted(dr.fieldnames) == sorted(field_names)
        for i, sample in enumerate(dr):
            if burnin and burnin > i:
                _LOG.debug('sample {0} from {1}... ignoring'.format(i+1, log.name))
                continue
            _LOG.debug('sample {0} from {1}... parsing'.format(i+1, log.name))
            count += 1
            for k in parameter_sums.iterkeys():
                parameter_sums[k] += float(sample[k])
        log.close()
    for k, v in parameter_sums.iteritems():
        parameter_sums[k] = v / count
    return parameter_sums, count

def get_master_mean_rate(beast_xml_file_path, beast_log_file_paths,
        header_offset=2, burnin=0):
    xml = open(beast_xml_file_path, 'rU')
    xml_tree = ElementTree.parse(xml)
    clock_sites = parse_clock_sites_from_xml(xml_tree)
    clock_patterns = [re.compile(k) for k in clock_sites.iterkeys()]
    clock_means, sample_size = get_parameter_means(
            logs = beast_log_file_paths,
            delimiter = '\t',
            header_offset = header_offset,
            burnin = burnin,
            parameter_patterns = clock_patterns)
    _LOG.info('mean rates:\n\t{0}\n\tsample size: {1}'.format(
            '\n\t'.join([': '.join([
                    k,
                    str(v),
                    str(clock_sites[k]),
                    ]) for k, v in clock_means.iteritems()]),
            sample_size))
    return weighted_mean(clock_means, clock_sites), sample_size

def weighted_mean(value_dict, weight_dict):
    assert sorted(value_dict.keys()) == sorted(weight_dict.keys())
    value_sum = 0.0
    weight_sum = 0.0
    for k in value_dict.iterkeys():
        value_sum += value_dict[k] * weight_dict[k]
        weight_sum += weight_dict[k]
    return value_sum / weight_sum

def parse_clock_sites_from_xml(xml_tree):
    clock_alignments = get_per_clock_alignments(xml_tree)
    clock_sites = {}
    for clock, alignments in clock_alignments.iteritems():
        sites = 0
        for alignment in alignments:
            sites += len(alignment.find('sequence').find('taxon').tail.strip())
        clock_sites[clock] = sites
    return clock_sites

def get_per_clock_alignments(xml_tree):
    clock_alignments = {}
    clock_patterns = get_per_clock_patterns(xml_tree)
    for clock, patterns in clock_patterns.iteritems():
        alignments = set()
        for p in patterns:
            alignment_ids = [el.attrib['idref'] for el in p.findall(
                    'alignment')]
            alignments.update(
                    [el for el in xml_tree.findall(
                            'alignment') if el.attrib['id'] in alignment_ids])
        if not clock in clock_alignments.keys():
            clock_alignments[clock] = set()
        clock_alignments[clock].update(alignments)
    return clock_alignments

def get_per_clock_patterns(xml_tree):
    tree_likelihoods = xml_tree.findall('treeLikelihood')
    clock_patterns = {}
    for tl in tree_likelihoods:
        patterns_list = tl.findall('patterns')
        clock_branch_rates_list = tl.findall('strictClockBranchRates')
        assert len(patterns_list) == 1
        assert len(clock_branch_rates_list) == 1
        patterns = None
        for el in xml_tree.iterfind('patterns'):
            if el.attrib['id'] == patterns_list[0].attrib['idref']:
                patterns = el
        clock_model = None
        for el in xml_tree.iterfind('strictClockBranchRates'):
            if el.attrib['id'] == clock_branch_rates_list[0].attrib['idref']:
                clock_model = el
        assert patterns is not None and clock_model is not None
        clock_parameter = clock_model.find('rate').find('parameter').attrib['id']
        if not clock_parameter in clock_patterns.keys():
            clock_patterns[clock_parameter] = set([patterns])
        else:
            clock_patterns[clock_parameter].add(patterns) 
    return clock_patterns


class NodeComment(object):
    comment_pattern = re.compile(r'([\w&%-]+)=[{]*([.\deE-]+)[, ]*([.\deE-]*)[}]*')
    age_pattern = re.compile(r'age', re.IGNORECASE)
    height_pattern = re.compile(r'height', re.IGNORECASE)
    length_pattern = re.compile(r'length', re.IGNORECASE)

    def __init__(self, node_comment):
        self._keys = []
        self._data = {}
        for match in self.comment_pattern.finditer(node_comment):
            assert len(match.groups()) == 3
            k, v1, v2 = match.groups()
            self._keys.append(k)
            if v2:
                self._data[k] = [float(v1), float(v2)]
            else:
                self._data[k] = [float(v1)]

    def __str__(self):
        data_strings = []
        for k in self._keys:
            if len(self._data[k]) == 2:
                s = '{0}={{{1},{2}}}'.format(k, self._data[k][0],
                        self._data[k][1])
            elif len(self._data[k]) == 1:
                s = '{0}={1}'.format(k, self._data[k][0])
            else:
                raise Exception('Node comment attribute with {0} '
                                'elements'.format(len(self._data[k])))
            data_strings.append(s)
        return ','.join(data_strings)

    def scale(self, multiplier):
        for k, v in self._data.iteritems():
            if self.age_pattern.search(k) or self.height_pattern.search(k) or self.length_pattern.search(k):
                self._data[k] = [x * multiplier for x in v]

def scale_tree(tree, multiplier):
    t = dendropy.Tree(tree)
    t.scale_edges(multiplier)
    for node in t.postorder_node_iter():
        new_comments = []
        for comment in node.comments:
            nc = NodeComment(comment)
            nc.scale(multiplier)
            new_comments.append(str(nc))
        node.comments = new_comments
    return t

def parse_rates(rates_file):
    f = open(rates_file, 'rU')
    header = f.next()
    rates = {}
    for line in f:
        l = line.strip().split()
        assert len(l) == 2
        rates[l[0]] = float(l[1])
    return rates

def main():
    mrc_paths = glob.glob(os.path.join(project_util.BEAST_DIR,
        '*', '*.mrc.tre'))
    rates_path = os.path.join(project_util.BIN_DIR, 'mutation_rates.txt')
    _LOG.info('parsing rates file {0}...'.format(rates_path))
    rates = parse_rates(rates_path)
    for path in mrc_paths:
        taxon = os.path.basename(os.path.dirname(path))
        xml_paths = glob.glob(os.path.join(project_util.BEAST_DIR, taxon, '*', '*.xml'))
        log_paths = glob.glob(os.path.join(project_util.BEAST_DIR, taxon, '*', '*.log'))
        assert len(xml_paths) == len(log_paths) == 4
        _LOG.info('parsing xml and log files to calc mean rate for {0!r}...'.format(
                taxon))
        mean_rate, sample_size = get_master_mean_rate(
                beast_xml_file_path = xml_paths[0],
                beast_log_file_paths = log_paths,
                header_offset = 2,
                burnin = 1001)
        branch_multiplier = 1 / (rates[taxon] * mean_rate)
        _LOG.info('\tmean rate = {0}'.format(mean_rate))
        _LOG.info('\tmutation rate = {0}'.format(rates[taxon]))
        _LOG.info('\tbranch multiplier = {0}'.format(branch_multiplier))
        tree = dendropy.Tree.get_from_path(path, schema='nexus')
        new_tree = scale_tree(tree, branch_multiplier)
        out_path = os.path.join(os.path.dirname(path), taxon + '.mrc.scaled.tre')
        new_tree.write_to_path(out_path, 'nexus')

if __name__ == '__main__':
    main()

