#! /usr/bin/env python

import os
import sys
import subprocess
import glob
import tempfile
import shutil
from cStringIO import StringIO

from Bio import SeqIO

from seqsift.seqops.seqfilter import row_filter, column_filter
from seqsift.utils.dataio import get_seq_iter

import project_util

_LOG = project_util.get_logger(__name__, level='info')

def expand_path(path):
    return os.path.abspath(os.path.expandvars(os.path.expanduser(path)))

def exe_paup(input_path, paup_path='paup'):
    cwd = os.path.abspath('.')
    if isinstance(input_path, str):
        stdin = open(input_path, 'rU')
    else:
        stdin = input_path
        input_path = input_path.name
    os.chdir(os.path.dirname(input_path))
    cmd = [paup_path,'-n']
    p = subprocess.Popen(cmd, shell=False,
            stdin=stdin,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
    o, e = p.communicate()
    exit_code = p.wait()
    stdin.close()
    # if not exit_code == 0:
    #     raise Exception('paup exited with code {0}\n'
    #             'here is the standard error:\n{1}\n'.format(exit_code, e))
    _LOG.debug('{0!r} paup stdout:\n{1}\n'.format(
            os.path.basename(input_path),
            o))
    _LOG.info('{0!r} paup stderr:\n{1}\n'.format(
            os.path.basename(input_path),
            e))
    os.chdir(cwd)

def filter_seqs(in_path, out_path,
        format='nexus',
        data_type='dna',
        character_list=['?', '-', 'N', 'n'],
        max_frequency=1.0):
    seqs = get_seq_iter([in_path], format=format, data_type=data_type)
    clean_seqs = column_filter(seqs, character_list=character_list,
            max_frequency=max_frequency)
    out = tempfile.NamedTemporaryFile()
    SeqIO.write(clean_seqs, handle=out, format='nexus')
    out.seek(0)
    paup_in = tempfile.NamedTemporaryFile()
    paup_in.write('#NEXUS\n')
    paup_in.write('begin paup;\n')
    paup_in.write('execute {0};\n'.format(expand_path(out.name)))
    paup_in.write('export format=nexus file={0};\n'.format(expand_path(out_path)))
    paup_in.write('end;\n')
    paup_in.seek(0)
    exe_paup(paup_in)
    out.close()


def main():
    paup_block_paths = glob.glob(os.path.join(project_util.RAW_DATA_DIR,
            '*.pblock.txt'))
    # execute paup blocks to create "clean" alignments
    for p in paup_block_paths:
        exe_paup(p)

    # filter out missing data from new alignments
    new_nexus_paths = glob.glob(os.path.join(project_util.DATA_DIR,
            '*.nex'))
    for p in new_nexus_paths:
        new_path = tempfile.NamedTemporaryFile()
        filter_seqs(p, new_path.name)
        new_path.seek(0)
        with open(p, 'w') as out:
            out.write(new_path.read())
        new_path.close()

if __name__ == '__main__':
    main()


