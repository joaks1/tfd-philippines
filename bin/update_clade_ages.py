#! /usr/bin/env python

import os
import sys
import re
import glob
from optparse import OptionParser

import dendropy
from goopy.gdoc import GoogleSpreadsheetClient

from rescale_trees import NodeComment
import project_util

_LOG = project_util.get_logger(__name__, level='info')

VALID_GROUPS = ['frog', 'squamate', 'bird', 'mammal']
VALID_TYPES = ['geographic',
               'singleton',
               'sympatric',
               'assembled']
FIELDNAMES = ['split', 'group', 'age', 'lower_CI', 'upper_CI', 'num_species',
        'type']

class CladeInfo(object):
    def __init__(self, taxon, idx, group, age, lower_ci, upper_ci, num_species):
        self.taxon = taxon
        self.idx = idx
        self.group = group
        self.age = age
        self.lower_ci = lower_ci
        self.upper_ci = upper_ci
        self.num_species = num_species

    def _get_name(self):
        return self.taxon + self.idx

    name = property(_get_name)

    def get_dict(self):
        return {'split': self.name,
                'age': self.age,
                'group': self.group,
                'lower_CI': self.lower_ci,
                'upper_CI': self.upper_ci,
                'num_species': self.num_species}

def parse_clade_info(username, password, spreadsheet_name, trees):
    taxon_pattern = re.compile('^([a-zA-Z]+)(\d*)$')
    gsc = GoogleSpreadsheetClient(
            username = username,
            password = password,
            spreadsheet_name_or_key = spreadsheet_name)
    clade_data = []
    for i, row in enumerate(gsc.rows):
        m = taxon_pattern.match(row.custom['taxa'].text)
        if not m:
            raise Exception('invalid taxon name {0} at row {1}'.format(
                    row.custom['taxa'].text, i+1))
        taxon, idx = m.groups()
        tips = [row.custom['tip1'].text, row.custom['tip2'].text]
        nspecies = row.custom['numspecies'].text
        group = row.custom['group'].text
        if any(not t for t in tips) or not taxon or not nspecies or not group:
            _LOG.warning('missing data for taxon {0} in row {1}'
                    '... skipping!'.format(taxon, i+1))
            continue
        tree = trees[taxon]
        if any (t not in tree.taxon_set.labels() for t in tips):
            raise ValueError('tip {0} and/or {1} not in tree for '
                    'taxon {2}'.format(tips[0], tips[1], taxon))
        mrca_node = tree.mrca(taxon_labels=tips)
        stem_node = mrca_node.parent_node
        assert mrca_node.level() > 0, 'mrca for {0} is root!'.format(taxon+idx)
        assert len(mrca_node.adjacent_nodes()) == 3, ('mrca for {0} is not '
                'bifurcation!'.format(taxon+idx))
        mrca_node_info = NodeComment(','.join(mrca_node.comments))
        stem_node_info = NodeComment(','.join(stem_node.comments))
        age = (mrca_node.distance_from_tip() \
                + stem_node.distance_from_tip()) / float(2)
        lower_ci, upper_ci = None, None
        hpd_patterns = [re.compile(r'^[&]*height_95%_HPD$'),
                        re.compile(r'^[&]*age_hpd95$')]
        for p in hpd_patterns:
            if not lower_ci:
                k_list = [k for k in mrca_node_info._data.keys() if p.match(k)]
                if len(k_list) == 1:
                    k = k_list[0]
                    lower_ci = min(mrca_node_info._data[k])
            if not upper_ci:
                k_list = [k for k in stem_node_info._data.keys() if p.match(k)]
                if len(k_list) == 1:
                    k = k_list[0]
                    upper_ci = max(stem_node_info._data[k])
        if not lower_ci or not upper_ci:
            raise Exception('could not parse node age HPDs for {0}'.format(
                    taxon+idx))
        clade_data.append(CladeInfo(
                taxon = taxon,
                idx = idx,
                group = group,
                age = age,
                lower_ci = lower_ci,
                upper_ci = upper_ci,
                num_species = int(nspecies)))
    return clade_data 

def parse_trees():
    trees = {}
    tree_paths = glob.glob(os.path.join(project_util.BEAST_DIR,
        '*', '*.scaled.tre'))
    for path in tree_paths:
        taxon = os.path.basename(os.path.dirname(path))
        trees[taxon] = dendropy.Tree.get_from_path(path, schema='nexus',
                preserve_underscores=True)
    return trees


def main():
    parser = OptionParser()
    parser.add_option('--not-dry', dest='dry', default=True,
            action='store_false',
            help='Not a dry run!')
    parser.add_option("-u", "--username", dest="username", 
        type="string",
        help="Google username.")
    parser.add_option("-p", "--password", dest="password", 
        type="string",
        help="Google password.")
    (options, args) = parser.parse_args()

    if args:
        _LOG.error('Script does not except any arguments')
        sys.stderr.write(str(parser.print_help()))
        sys.exit(1)

    clade_def_spreadsheet_name = 'philippine_herp_clade_definitions'
    clade_age_spreadsheet_name = 'Philippine_clade_ages'
    trees = parse_trees()

    dry_run = options.dry

    clade_data = parse_clade_info(
            username = options.username,
            password = options.password,
            spreadsheet_name = clade_def_spreadsheet_name,
            trees = trees)
    # import code; code.interact(local=locals());
    gsc = GoogleSpreadsheetClient(
            username = options.username,
            password = options.password,
            spreadsheet_name_or_key = clade_age_spreadsheet_name)
    filter_headers = ['split']

    for clade in clade_data:
        gsc.update_sheet_by_row(
                filter_headers = filter_headers,
                filter_pattern = clade.name,
                new_row_dict = clade.get_dict(),
                insert = True,
                dry_run = dry_run)
    
    clade_data_path = os.path.join(project_util.BIN_DIR, 'clade_age_data.txt')
    if not dry_run:
        gsc.download_spreadsheet(clade_data_path,
                fieldnames = FIELDNAMES,
                delimiter = '\t')

if __name__ == '__main__':
    main()

