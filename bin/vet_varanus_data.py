#! /usr/bin/env python

import os
import sys
import subprocess
import glob
import shutil

from Bio import SeqIO

from seqsift.seqops.seqmod import translate_seqs
from seqsift.utils.dataio import get_seq_iter

import project_util

_LOG = project_util.get_logger(__name__, level='info')

def main():
    paths = glob.glob(os.path.join(project_util.DATA_DIR,
            'varanus.nd*.nex'))
    for p in paths:
        new_path = p.replace('nex', 'aa.fasta')
        seqs = get_seq_iter(p, format='nexus', data_type='dna')
        aa_seqs = translate_seqs(seqs, gap_characters=['-', '?'],
                table="Vertebrate Mitochondrial")
        SeqIO.write(aa_seqs, handle=new_path, format='fasta')

if __name__ == '__main__':
    main()


