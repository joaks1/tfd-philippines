#NEXUS

begin paup;
    execute Pseudogekko.nex;
    include ND2 / only;
    export format=nexus file=../pseudogekko.nd2.nex;
    include tRNAs / only;
    export format=nexus file=../pseudogekko.trnas.nex;
end;

