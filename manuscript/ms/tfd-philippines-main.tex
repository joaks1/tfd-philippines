\section*{Introduction}
The Islands of the Philippines may harbor the highest concentration of
biodiversity on Earth, and a large proportion of this diversity is comprised of
endemic species \citep{Rafe2009,Heaney2007}.
Over the time this biodiversity was generated, the islands have experienced a
long, dynamic, and relatively well-understood geological history
\citep{Hall1998,Voris2000,Yumul2008}.
The combination of their biodiversity and dynamic history makes the islands an
important model system for investigating how large-scale geological and
climatic processes generate and maintain biodiversity.

The Philippines are comprised of both continental and oceanic islands.
The former comprise the Palawan Microcontinental Block, which broke away from
mainland Asia and were potentially submerged \citep[but
see][]{Blackburn2010,Siler2012} as they moved southward before re-emerging as
islands.
The latter comprise the Philippine Mobile Belt and originated via volcanism
much further east in the Pacific as the Philippine Sea Plate moved westward,
eventually colliding with the Palawan Microcontinental Block, bringing the
islands of the Philippines to their current position just east of the Sunda
Shelf \citep{Hall1996,Hall1998,Yumul2008,Yumul2009}.

Our goal is to establish broad-scale temporal patterns of colonization and
diversification across a diverse set of vertebrates within the dynamic
geological framework that gave rise to the Philippine Islands.
We assimilate a large comparative dataset of colonization-time estimates for a
diverse set of monophyletic groups of vertebrates that all reside in the
Philippines.
We use these data to estimate how much of the Philippines' species diversity is
explained by the amount of time lineages have had to diversify within the
archipelago (i.e., a time-for-diversification effect), and test whether this
accounts for a significant proportion of variation in diversity
\citep{Wiens2009,StephensWiens2003}.
Furthermore, we compare patterns of colonization time and diversity across
major vertebrate groups and discuss how they might help explain some of the
general patterns of biodiversity in the islands.

\section*{Methods}
We gathered divergence time estimates for clades of Philippine vertebrates from
the literature.
We also gathered available sequence datasets that include Philippine taxa for
estimating divergence times for additional Philippine groups.
% We gathered estimates from the literature, and data to obtain estimates, of the
% ages of vertebrate clades in the Philippines.
We considered all groups of two or more species that are estimated to form a
monophyletic group of Philippine species.
In particular, we were interested in estimating the time these clades colonized
the archipelago.
Whenever possible, we approximated the time of colonization of the Philippines
using the midpoint between the estimated ages of the node of the most-recent
common ancestor (MRCA) of the Philippine clade (i.e., the crown-based node) and
the node of the MRCA of the Philippine clade and its most closely related
non-Philippine relative(s) (i.e., the stem-based node).
We excluded taxa with only a single species in the Philippines due to 
the inability to establish a lower bound for the time of invasion for
these taxa, other than the present \citep{Wiens2009}.
Whenever possible, we also approximated a confidence interval (CI) for each
colonization time, which extends from the lower limit of the estimated CI for
the age of the crown-based node up to the upper limit of the CI for the age of
the stem-based node.
When CIs were not available, we used the estimated crown and stem node ages for
the bounds.
Any exceptions to how we established either the midpoint or CIs are noted
below.
All of the estimates of invasion times, along with the estimates of species
diversity of each clade, are summarized in Table \ref{tabCladeAges}.  We used
BEAST \citep[v1.7.5;][]{Drummond2007} for all dating analyses, the full details
of which, including the XML-formatted analysis files, can be found at
\href{https://joaks1@bitbucket.org/joaks1/tfd-philippines.git}{https://joaks1@bitbucket.org/joaks1/tfd-philippines.git}.

\subsection*{Colonization ages of avian clades}
We use node-age estimates of two Philippine clades of \emph{Copsychus}
from \citet{Lim2010}.
This includes the clade comprised of \emph{C.~niger} and \emph{C.~cebuensis},
and the clade comprised of \emph{C.~luzoniensis} and \emph{C.~superciliaris}
(Table \ref{tabCladeAges}).
We use the age estimates from \citet{Lim2010} that are based on the rate
distribution of \citet{Nabholz2009} for sites at the third-codon position
within the cytochrome-b gene.

Furthermore, we use the ages of:
(1) a three-species clade of the \emph{Irena cyanogastra} group as estimated by
\citet{Moltesen2012};
(2) a clade comprised of \emph{Hypothymis helenae} and \emph{H.~coelestis} as
estimated by \citet{Fabre2012} (we use the age estimate and CI for the
stem-based node of this clade, because an age estimate for the crown-based node
is not provided);
(3) a clade comprised of \emph{Terpsiphone unirufa} and \emph{T.~cinnamomea}
also estimated by \citet{Fabre2012} (the age estimate and CI for both the crown
and stem nodes were provided for this group).
(4) a nine-species clade of \emph{Aethopyga} as estimated by \citet{Hosner2013}
(we use the results of their dating analysis that was calibrated with a 2.4\%
divergence rate per million years);
(5) a clade comprised of \emph{Oriolus steerei} and \emph{O.~isabellae} as
estimated by \citet{Jonsson2010};
(6) a three-species clade of \emph{Rhabdornis} as estimated by
\citet{Zuccon2006} (we use the CI reported for the age of the stem-based node
and its midpoint as the colonization-time estimate, because age information for
the crown-based node in not provided).

We re-analyzed the ND2 data of \citet{Oliveros2010}, \citet{Oliveros2012}, and
\citet{SanchezGonzalez2011} using a strict-clock model with a mutation rate of
1.2\% per million years for all three datasets.
From our results, we use the estimated ages of 
(1) a seven-species clade of \emph{Hypsipetes},
(2) a three-species clade of \emph{Robsonius},
and
(3) a six-species clade of \emph{Rhipidura} (Table \ref{tabCladeAges}).

\subsection*{Colonization ages of mammalian clades}
We use the age of an eight-species clade of \emph{Crocidura} as estimated by
\citet{Esselstyn2009MPE} using their combined calibration strategy.
We assume that the unsampled species \emph{C.~grandis} is a member of this
clade.
We use the age estimates of \citet{Jansa2006} for five clades (labeled A--E by
\citet{Jansa2006}) of Murinae.
We use the results of their Bayesian analyses of the IRBP locus; we use only
point estimates of the stem-based nodes as proxies for invasion-age estimates,
as this was the only information provided for these five clades.

\subsection*{Colonization ages of anuran clades}
We use the age of a nine-species clade of \emph{Sanguirana} as estimated by
\citet{Wiens2009}; we use the crown and stem-based nodes as the lower and upper
limits of our CI, respectively, because no CIs were provided.

Furthermore, we analyzed the data of \citet{Blackburn2013}, \citet{Rafe2013},
and \citet{Evans2003} to estimate the ages of a 
(1) 14-species clade of \emph{Kaloula},
(2) six-species clade of \emph{Hylarana},
and
(3) an eight-species clade of \emph{Limnonectes}, respectively (Table
\ref{tabCladeAges}).

\subsection*{Colonization ages of squamate clades}
We use the ages of two clades of \emph{Sphenomorphus}-group skinks as estimated
by \citet{Rafe2013AREES}, and a 13-species clade of \emph{Gekko} as estimated
by \citet{Siler2012}.
Furthermore, we re-analyze the following datasets to estimate clade
colonization ages:
(1) \cite{Barley2013} to obtain an estimate for a 12-species clade of
\emph{Eutropis},
(2) \citet{Siler2013} to obtain an estimate for the 9-species clade of
\emph{Lycodon},
(3) \citet{Siler2011} to obtain an estimate of a 41-species clade of
\emph{Brachymeles},
(4) \citet{Siler2010} to obtain an estimates of a 12-species clade of
\emph{Cyrtodactylus},
(5) \citet{Rafe2012Ptych} and \citet{Rafe2012Lup} to estimate the age of a
six-species clade of \emph{Pseudogekko},
(6) and unpublished data to estimate the age of an 11-species clade of
\emph{Gonocephalus} (Table \ref{tabCladeAges}).
The assumed rates of substitution used in these analyes are summarized in Table
\ref{tabRates}, and full details of the BEAST analyses can be found at
\href{https://joaks1@bitbucket.org/joaks1/tfd-philippines.git}{https://joaks1@bitbucket.org/joaks1/tfd-philippines.git}.

\subsection*{Evaluating the time-for-diversification hypothesis}
We plot the clades' estimated times of colonization of the Philippines against
diversity, using both the raw and log-transformed number of species.
Given that diversity will increase exponentially under a model with a constant
positive diversification rate across lineages, log-transforming diversity
should linearize the relationship between diversity and time.
We use least-squares linear regression to estimate the proportion of the
variation in species diversity explained by colonization time, and to test the
significance of the relationship.

\section*{Results}
Using the raw data, we are unable to reject a null hypothesis of no
relationship between time and species diversity with a Type I error rate of
0.05 ($P = 0.068$; Figure \ref{figAgeVsDiversity}).
After log-transforming the number of species for each clade, the
relationship is significant ($P = 0.007$; Figure \ref{figAgeVsDiversity}).
However, even when diversity is log-transformed, the time for clade
diversification only explains 23\% of the variance in diversity among the
groups we analyzed.
One interesting pattern that emerges from our results is that all
of the squamate groups we analyzed are estimated to have colonized the
Philippine Islands before any of the avian, mammalian, or frog groups
(Figure~\ref{figAgeVsDiversity}).


\section*{Discussion}
Our results suggest a relatively weak relationship between the time of
colonization of the islands and present-day species diversity
(Figure~\ref{figAgeVsDiversity}).
We expected to find a large amount of unexplained variation of diversity.
A portion of this unexplained variance is likely due to the stochastic nature
of diversification processes.
For example, even simple, constant-rate birth-death models, where diversity is a
direct function of time, predict a large amount of variance in diversity.
Furthermore, there are many deterministic factors (e.g., vagility, range size,
and other ecological factors) that also likely account for much of the
unexplained variation in diversity.

Perhaps more interesting than the relatively weak time-for-diversification
effect, is our finding that of all the vertebrate lineages we examined, all of
the squamate taxa are estimated to have arrived in the Philippines before any
of the other groups, including birds, mammals, and frogs
(Figure~\ref{figAgeVsDiversity}).
The colonization times for some of the lizard groups (\emph{Eutropis},
\emph{Gekko}, and \emph{Pseudogekko}) are likely over estimates (Table
\ref{tabCladeAges}) due to long branches connecting the Philippine species
to their nearest non-Philippine relatives.
Thus, the midpoint between the ages of the crown and stem nodes for these groups is
likely a poor estimate of their colonization time.
Nonetheless, even the lower limits of the CIs on the time of colonization for
these groups (which are based on the age of the crown node) are still older
than any of the non-squamate taxa.
(Figure~\ref{figAgeVsDiversity}).

This pattern of early squamate invasion is interesting given the geological
history of the islands.
The islands of the Philippine Mobile Belt are of volcanic origin and formed on
the Philippine Sea Plate further out in the Pacific and subsequently moved
westward toward their current location just east of the Sunda Shelf
\citep{Hall1996,Hall1998,Yumul2008,Yumul2009}.
Thus, many of the islands spent much of their history as remote islands, much
farther from any continental sources of biodiversity.
Given that lizards are presently a main component of the vertebrate fauna of
remote Pacific Islands, it is not surprising to see that they were early
colonizers of the Philippines when the islands were much more isolated.

Given the history of the islands and the relative dispersal abilities of the
groups, we might expect lizards to be early colonizers relative to mammals and
frogs.
However, it is somewhat surprising that none of the bird groups we included are
early colonizers of the islands given their high vagility and the fact that
birds are also common across remote islands of the Pacific.
Perhaps this pattern is a reflection of their high dispersal ability driving a
higher turnover of avian biodiversity in the islands.
This could lead to many of the early avian colonizers being replaced by more
recent colonizers as the islands approached the Sunda Shelf.
However, this is speculative, and the groups we include in this study are
certainly not a random sample of vertebrate biodiversity of the Philippines.
Furthermore, our methods for approximating the times of colonization make many
simplifying assumptions that are likely violated across these groups.
Nonetheless, our goal is to establish a broad picture of the temporal patterns
of colonization and diversification using as much information that is currently
available.
We suspect that some of the general patterns in our results, such as the
time-for-diversification effect and early squamate colonization, are relatively
robust to biased taxon sampling and model violations.

